﻿using pre.Domain.Models;
using System.Threading.Tasks;

namespace pre.Bussiness.Interfaces
{
    public interface ITopicoService
    {
        Task<JsonResponse<Topico>> AddTopico(Topico topico);
    }
}
