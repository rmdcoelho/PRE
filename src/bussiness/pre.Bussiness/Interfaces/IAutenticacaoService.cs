﻿using pre.Domain.Models;

namespace pre.Bussiness.Interfaces
{
    public interface IAutenticacaoService
    {
        bool CredenciaisValidas(Usuario usuarioInput, Usuario usuarioDB);
    }
}
