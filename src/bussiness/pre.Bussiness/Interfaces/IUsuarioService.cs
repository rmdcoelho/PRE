﻿using pre.Domain.Models;
using pre.MapperEntities.ViewModels.Usuario;

namespace pre.Bussiness.Interfaces
{
    public interface IUsuarioService
    {
        UsuarioResponseVM Find(Usuario usuario);
    }
}
