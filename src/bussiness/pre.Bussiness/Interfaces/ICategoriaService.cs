﻿using pre.Domain.Models;
using pre.MapperEntities.ViewModels.Categoria;
using System.Threading.Tasks;

namespace pre.Bussiness.Interfaces
{
    public interface ICategoriaService
    {
        Task<JsonResponse<CategoriaInputVM>> AddCategoria(CategoriaInputVM categoria);
    }
}
