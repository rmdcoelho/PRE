﻿using pre.Bussiness.Interfaces;
using pre.Domain.Models;
using pre.Repository.interfaces;
using System.Threading.Tasks;

namespace pre.Bussiness.Services
{
    public class TopicoServices : ITopicoService
    {
        private ITopicoRepository _topicoRepository;

        public TopicoServices(ITopicoRepository topicoRepository)
        {
            _topicoRepository = topicoRepository;
        }

        public async Task<JsonResponse<Topico>> AddTopico(Topico topico)
        {
            var isSuccess = false;
            var message = "Ocorreu um erro ao salvar";
            var result = await _topicoRepository.AddTopico(topico);

            if(null != result)
            {
                isSuccess = true;
                message = "Salvo com sucesso";
            }

            return new JsonResponse<Topico>
            {
                Objeto = result,
                Message = message,
                Success = isSuccess
            };
        }
    }
}
