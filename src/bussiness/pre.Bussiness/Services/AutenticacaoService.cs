﻿using pre.Bussiness.Interfaces;
using pre.Domain.Models;

namespace pre.Bussiness.Services
{
    public class AutenticacaoService : IAutenticacaoService
    {
        public bool CredenciaisValidas(Usuario usuarioInput, Usuario usuarioDB)
        {
            return (usuarioDB != null && usuarioInput.Username == usuarioInput.Username &&
                    usuarioInput.AccessKey == usuarioDB.AccessKey);
        }
    }
}
