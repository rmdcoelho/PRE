﻿using AutoMapper;
using pre.Bussiness.Interfaces;
using pre.Domain.Models;
using pre.MapperEntities.ViewModels.Categoria;
using pre.Repository.interfaces;
using System;
using System.Threading.Tasks;

namespace pre.Bussiness.Services
{
    public class CategoriaService : ICategoriaService
    {
        private ICategoriaRepository _categoriaRepository;
        private IMapper _mapper;

        public CategoriaService(ICategoriaRepository categoriaRepository, IMapper mapper)
        {
            _categoriaRepository = categoriaRepository;
            _mapper = mapper;
        }

        public async Task<JsonResponse<CategoriaInputVM>> AddCategoria(CategoriaInputVM categoriaInput)
        {
            var isSuccess = false;
            var message = "Ocorreu um erro ao salvar";

            var categoria = _mapper.Map<Categoria>(categoriaInput);

            //TRY
            var result = await _categoriaRepository.AddCategoria(categoria);

            if (null != result)
            {
                isSuccess = true;
                message = "Salvo com sucesso";
            }

            return new JsonResponse<CategoriaInputVM>
            {
                Objeto = categoriaInput,
                Message = message,
                Success = isSuccess
            };
        }
    }
}
