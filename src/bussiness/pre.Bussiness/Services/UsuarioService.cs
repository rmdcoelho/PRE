﻿using pre.Bussiness.Interfaces;
using pre.Domain.Models;
using pre.MapperEntities.ViewModels.Usuario;
using pre.Repository.repositories;

namespace pre.Bussiness.Services
{
    public class UsuarioService : IUsuarioService
    {
        private UsuarioRepository _usuarioRepository;
        private IAutenticacaoService _autenticacaoService;

        public UsuarioService(UsuarioRepository usuarioRepository, IAutenticacaoService autenticacaoService)
        {
            _usuarioRepository = usuarioRepository;
            _autenticacaoService = autenticacaoService;
        }

        public UsuarioResponseVM Find(Usuario usuario)
        {
            var usuarioDb = _usuarioRepository.Find(usuario.Username);
            bool autenticado = false;

            if (usuario != null && usuarioDb != null)
            {
                autenticado = _autenticacaoService.CredenciaisValidas(usuario, usuarioDb);

                //usar o viewMapper:
                return new UsuarioResponseVM()
                {
                    Nome = usuarioDb?.Nome,
                    Sobrenome = usuarioDb?.Sobrenome,
                    Username = usuarioDb?.Username,
                    Ativo = usuarioDb.Ativo,
                    Nascimento = usuarioDb?.Nascimento.ToString("dd/MM/yyyy"),
                    Autenticado = autenticado
                };
            }
            return new UsuarioResponseVM();
        }
    }
}
