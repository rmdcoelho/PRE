﻿using System;

namespace pre.Domain.Models
{
    public class Tipo
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
    }
}
