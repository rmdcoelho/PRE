﻿using System;

namespace pre.Domain.Models
{
    public class Topico
    {
        public Guid Id { get; set; }

        public Guid MateriaId { get; set; }
        public Guid TipoId { get; set; }

        public string Nome { get; set; }
        public DateTime UltimaRevisao { get; set; }
        public bool Revisado { get; set; }

        public Materia Materia { get; set; }
        public Tipo Tipo { get; set; }
    }
}
