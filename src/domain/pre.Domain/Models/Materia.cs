﻿using System;

namespace pre.Domain.Models
{
    public class Materia
    {
        public Guid Id { get; set; }
        public Guid CategoriaID { get; set; }

        public string Nome { get; set; }

        public Categoria Categoria { get; set; }
    }
}
