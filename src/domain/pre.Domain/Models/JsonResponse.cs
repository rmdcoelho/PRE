﻿namespace pre.Domain.Models
{
    public class JsonResponse<T> where T : class
    {
        public T Objeto { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
