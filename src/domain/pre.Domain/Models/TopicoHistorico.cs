﻿using System;

namespace pre.Domain.Models
{
    public class TopicoHistorico
    {
        public Guid Id { get; set; }
        public Guid TopicoId { get; set; }

        public DateTime DataRevisao { get; set; }

        public Topico Topico { get; set; }
    }
}
