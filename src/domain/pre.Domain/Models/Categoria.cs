﻿using System;

namespace pre.Domain.Models
{
    public class Categoria
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
    }
}
