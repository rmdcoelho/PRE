﻿using System;

namespace pre.Domain.Models
{
    public class Usuario
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string AccessKey { get; set; }
        public bool Ativo { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public DateTime Nascimento { get; set; }
    }
}
