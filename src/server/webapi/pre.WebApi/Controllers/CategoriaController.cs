﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pre.Bussiness.Interfaces;
using pre.Domain.Models;
using pre.MapperEntities.ViewModels.Categoria;
using System.Threading.Tasks;

namespace pre.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CategoriaController : Controller
    {
        //[Authorize("Bearer")]
        [HttpPost("add")]
        public async Task<JsonResponse<CategoriaInputVM>> Post(
            [FromBody]CategoriaInputVM categoriaInput,
            [FromServices]ICategoriaService categoriaService)
        {
            return await categoriaService.AddCategoria(categoriaInput);
        }
    }
}