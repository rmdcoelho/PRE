﻿namespace pre.MapperEntities.ViewModels.Usuario
{
    public class UsuarioResponseVM
    {
        public string Username { get; set; }
        public bool Ativo { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Nascimento {get; set;}
        public bool Autenticado { get; set; }
    }
}
