﻿using System;

namespace pre.MapperEntities.ViewModels.Categoria
{
    public class CategoriaInputVM
    {
        public CategoriaInputVM()
        {

        }

        public CategoriaInputVM(string nome)
        {
            Nome = Nome;
        }

        //REMOVER ID e arrumar configuração do automapper
        public Guid Id { get; set; }
        public string Nome { get; set; }
    }
}
