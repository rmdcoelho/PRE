﻿using AutoMapper;
using pre.Domain.Models;
using pre.MapperEntities.ViewModels.Categoria;

namespace pre.MapperEntities.ConfigProfile.CategoriaProfile
{
    public class CategoriaConfigProfile : Profile
    {
        public CategoriaConfigProfile()
        {
            CreateMap<CategoriaInputVM, Categoria>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());
        }
    }
}
