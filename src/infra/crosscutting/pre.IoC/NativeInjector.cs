﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using pre.Bussiness.Interfaces;
using pre.Bussiness.Services;
using pre.Repository.interfaces;
using pre.Repository.repositories;
using System;
using System.Reflection;

namespace pre.IoC
{
    public static class NativeInjector
    {
        public static IServiceProvider RegisterServices(this IServiceCollection services)
        {
            //Configuration:
            services.AddAutoMapper();

            //Services:
            services.AddTransient<IUsuarioService, UsuarioService>();
            services.AddTransient<IAutenticacaoService, AutenticacaoService>();
            services.AddTransient<ITopicoService, TopicoServices>();
            services.AddTransient<ICategoriaService, CategoriaService>();

            //Repositories:
            //services.AddTransient(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddTransient<ICategoriaRepository, CategoriaRepository>();
            services.AddTransient<IUsuarioRepository, UsuarioRepository>();
            services.AddTransient<ITopicoRepository, TopicoRepository>();

            return services.BuildServiceProvider();
        }
    }
}
