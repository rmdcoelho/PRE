﻿using pre.Domain.Models;
using pre.Repository.Contextos;
using pre.Repository.interfaces;
using System.Threading.Tasks;

namespace pre.Repository.repositories
{
    public class TopicoRepository : BaseRepository<Topico>, ITopicoRepository
    {
        public TopicoRepository(ContextoPostgresql ctx) : base(ctx) { }

        public async Task<Topico> AddTopico(Topico topico)
        {
            return await Add(topico);
        }
    }
}
