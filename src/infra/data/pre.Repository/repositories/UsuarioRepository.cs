﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using pre.Domain.Models;
using pre.Repository.interfaces;

namespace pre.Repository.repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private IConfiguration _configuration;

        public UsuarioRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Usuario Find(string username)
        {
            //using (SqlConnection conexao = new SqlConnection(
            using (NpgsqlConnection conexao = new NpgsqlConnection(
                _configuration.GetConnectionString("ConnectionString")))
            {
                var result = conexao.QueryFirstOrDefault<Usuario>(
                    "SELECT id, username, nome, accesskey, sobrenome, nascimento, ativo " +
                    "FROM usuario " +
                    "WHERE username = @Username", new { Username = username });

                return result;
            }
        }
    }
}
