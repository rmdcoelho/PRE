﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using pre.Repository.Contextos;
using pre.Repository.interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace pre.Repository.repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private ContextoPostgresql _ctx;
        private DbSet<TEntity> _dbSet;

        public BaseRepository(ContextoPostgresql ctx)
        {
            _ctx = ctx;
            _dbSet = ctx.Set<TEntity>();
        }

        public async Task<TEntity> Add(TEntity entity)
        {
            EntityEntry<TEntity> result = null;

            //TRY retornar bool
            result = _dbSet.Add(entity);
            await _ctx.SaveChangesAsync();

            return result.Entity;
        }

        public async Task<bool> Delete(TEntity entity)
        {
            var result = _dbSet.Remove(entity);
            await _ctx.SaveChangesAsync();
            return result != null;
        }

        public async Task<TEntity> Find(TEntity entity)
        {
            return await _dbSet.FindAsync(entity);
        }

        public async Task<ICollection<TEntity>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<bool> Update(TEntity entity)
        {
            var entidade = Find(entity);
            if (null != entidade)
            {
                _dbSet.Update(entity);
                await _ctx.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}
