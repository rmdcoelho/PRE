﻿using System.Threading.Tasks;
using pre.Domain.Models;
using pre.Repository.Contextos;
using pre.Repository.interfaces;

namespace pre.Repository.repositories
{
    public class CategoriaRepository : BaseRepository<Categoria>, ICategoriaRepository
    {
        public CategoriaRepository(ContextoPostgresql ctx) : base(ctx) { }

        public async Task<Categoria> AddCategoria(Categoria categoria)
        {
            return await Add(categoria); 
        }
    }
}
