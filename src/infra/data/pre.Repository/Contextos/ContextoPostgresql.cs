﻿using Microsoft.EntityFrameworkCore;
using pre.Domain.Models;
using pre.Repository.Mapping;

namespace pre.Repository.Contextos
{
    public class ContextoPostgresql : DbContext
    {
        public ContextoPostgresql(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Materia> Materias { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Tipo> Tipos { get; set; }
        public DbSet<TopicoHistorico> TopicosHistoricos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoriaConfiguration());
        }
    }
}
