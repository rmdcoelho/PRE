﻿using pre.Domain.Models;
using System.Threading.Tasks;

namespace pre.Repository.interfaces
{
    public interface ICategoriaRepository
    {
        Task<Categoria> AddCategoria(Categoria categoria);
    }
}
