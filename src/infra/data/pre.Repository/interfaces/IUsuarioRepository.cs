﻿using pre.Domain.Models;
using System;

namespace pre.Repository.interfaces
{
    public interface IUsuarioRepository
    {
        Usuario Find(string username);
    }
}
