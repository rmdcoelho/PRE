﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace pre.Repository.interfaces
{
    public interface IBaseRepository<TReponse> where TReponse : class
    {
        Task<TReponse> Add(TReponse entity);
        Task<TReponse> Find(TReponse entity);
        Task<ICollection<TReponse>> GetAll();
        Task<bool> Update(TReponse entity);
        Task<bool> Delete(TReponse entity);
    }
}
