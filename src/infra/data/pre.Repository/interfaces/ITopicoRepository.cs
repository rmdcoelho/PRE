﻿using pre.Domain.Models;
using System.Threading.Tasks;

namespace pre.Repository.interfaces
{
    public interface ITopicoRepository
    {
        Task<Topico> AddTopico(Topico topico);
    }
}
