import React from "react";
import ReactDom from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import App from "./App";
import AtualizarCategoria from "./components/categoria/atualizarcategoria.js";
import NavigationBar from "./components/navigationComponents/navigationBar.js";
import WindowButtons from "./components/navigationComponents/windowButtons.js";
import FormSelector from "./components/navigationComponents/formSelectors.js";
import { AtualizarTopico } from "./components/topico/atualizacaoTopico.js";

ReactDom.render(<App />, document.getElementById("root"));
