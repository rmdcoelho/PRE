import React, { Component } from "react";

class Aviso extends Component {
  state = {};

  render() {
    return (
      <div class="jumbotron">
        <h1 class="display-4">Bem vindo(a)!</h1>
        <p class="lead">Efetue o login caso já possua um cadastro.</p>
        <hr class="my-4" />
        <p>
          Registre-se para ter acesso as incríveis funcionalidades do sistema!
        </p>
        <a class="btn btn-primary btn-lg" href="#" role="button">
          Registrar
        </a>
      </div>
    );
  }
}

export default Aviso;
