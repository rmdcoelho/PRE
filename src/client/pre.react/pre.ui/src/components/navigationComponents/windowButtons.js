import React, { Component } from "react";

class WindowButtons extends Component {
  state = {};

  render() {
    return (
      <div
        style={{
          display: "flex",
          padding: "40px",
          flexDirection: "column"
        }}
      >
        <button className="btn btn-info" style={{ marginBottom: "10px" }}>
          Calendário
        </button>
        <button className="btn btn-info" style={{ marginBottom: "10px" }}>
          Diário
        </button>
        <button className="btn btn-info" style={{ marginBottom: "10px" }}>
          Revisões
        </button>
        <button className="btn btn-info" style={{ marginBottom: "10px" }}>
          Gerenciamento
        </button>
      </div>
    );
  }
}

export default WindowButtons;
