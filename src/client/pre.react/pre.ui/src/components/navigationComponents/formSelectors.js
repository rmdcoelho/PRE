import React, { Component } from "react";
import AtualizacaoCategoria from "../.././components/categoria/atualizarcategoria.js";

class FormSelectors extends Component {
  state = {};

  componentDidMount() {
    document
      .getElementById("categoria-form-radio")
      .addEventListener("change", function() {
        if (document.getElementById("categoria-form").checked) {
          document.getElementById("atualizar-categoria-form").style.display =
            "block";
        }
      });
  }

  render() {
    componentDidMount();

    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row"
        }}
        class="btn-group btn-group-toggle"
        data-toggle="buttons"
      >
        <label class="btn btn-info">
          <input
            type="radio"
            name="options"
            id="categoria-form-radio"
            autocomplete="off"
            checked
          />{" "}
          Categoria
        </label>
        <label class="btn btn-info">
          <input
            type="radio"
            name="options"
            id="topico-form"
            autocomplete="off"
          />{" "}
          Tópico
        </label>
        <label class="btn btn-info">
          <input
            type="radio"
            name="options"
            id="materia-form"
            autocomplete="off"
          />{" "}
          Matéria
        </label>
      </div>
    );
  }
}

export default FormSelectors;
