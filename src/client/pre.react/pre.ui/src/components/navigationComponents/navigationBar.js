import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class NavigationBar extends Component {
  state = {
    //logo: "%PUBLIC_URL%/rabbit.png"
  };

  render() {
    console.log(process.env);
    return (
      <div
        className="nav navbar"
        style={{
          display: "flex",
          justifyContent: "space-around",
          background: "#563d7c",
          padding: "15px 0px 15px 0px"
        }}
      >
        {/* <img
          style={{ marginLeft: "20px" }}
          src={require("./rabbit.png")}
          alt="Logo"
        /> */}
        <FontAwesomeIcon
          icon="glasses"
          style={{
            marginLeft: "20px",
            color: "white",
            marginRight: "15px",
            fontSize: "35px"
          }}
        />
        <span
          style={{
            flex: 10,
            textAlign: "left",
            marginLeft: "10px",
            fontWeight: "700",
            color: "white"
          }}
        >
          <i>Programa de Revisão de Estudos</i>
        </span>
        <button
          className="btn btn-light"
          style={{ flex: 1, textAlign: "center", marginRight: "20px" }}
        >
          Sair
        </button>
        <button
          className="btn btn-light"
          style={{ flex: 1, textAlign: "center", marginRight: "20px" }}
        >
          Login
        </button>
        <div class="dropdown" style={{ flex: 1 }}>
          <button
            class="btn btn-light dropdown-toggle"
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Perfil
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">
              Meus Dados
            </a>
            <a class="dropdown-item" href="#">
              Sobre
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default NavigationBar;
