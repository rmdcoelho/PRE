import React, { Component } from "react";

class AtualizarTopico extends Component {
  state = {};

  LimparFormulario() {
    document.getElementById("nome-topico").value = "";
    document.getElementById("tipo-topico").value = "";
    document.getElementById("materia-topico").value = "";
  }

  salvarTopico() {
    let nomeTopico = document.getElementById("nome-topico").value;
    let idTipoTopico = document.getElementById("tipo-topico").selectedValue;
    let idMateriaTopico = document.getElementById("materia-topico")
      .selectedValue;

    fetch("http://192.168.15.9:53721/api/topico/add", {
      method: "POST",
      //mode: "no-cors",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        Nome: nomeTopico,
        TipoId: idTipoTopico,
        MateriaId: idMateriaTopico
      })
    })
      .then(function(result) {
        console.log("ok");
        console.log(result);
      })
      .catch(function() {
        console.log("error");
      });
    console.log("executou salvar!");
  }

  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          background: "#e9ecef",
          padding: "20px"
          //display: "none"
        }}
      >
        <span
          style={{
            color: "#563d7c",
            fontSize: "20px",
            textAlign: "center",
            fontWeight: "700",
            marginBottom: "10px"
          }}
        >
          Tópico
        </span>
        <div className="form-group">
          <div className="row">
            <div className="col-lg-12">
              <label>Nome</label>
              <input
                id="nome-topico"
                className="form-control"
                style={{ marginBottom: "10px" }}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <label>Tipo</label>
              <select
                id="tipo-topico"
                className="form-control"
                style={{ marginBottom: "10px" }}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <label>Matéria</label>
              <select
                id="materia-topico"
                className="form-control"
                style={{ marginBottom: "10px" }}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <button
                className="btn btn-primary form-control"
                onClick={() => this.LimparFormulario()}
                style={{ marginTop: "10px" }}
              >
                Limpar
              </button>
            </div>
            <div className="col-md-6">
              <button
                className="btn btn-primary form-control"
                onClick={() => this.salvarTopico()}
                style={{ marginTop: "10px" }}
              >
                Salvar
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AtualizarTopico;
