import React, { Component } from "react";

class AtualizarCategoria extends Component {
  state = {};

  LimparFormulario() {
    document.getElementById("nome-categoria").value = "";
  }

  salvarCategoria() {
    let nomeCategoria = document.getElementById("nome-categoria").value;

    //fetch("http://localhost:53721/api/categoria/add", {
    fetch("http://192.168.15.9:53721/api/categoria/add", {
      method: "POST",
      //mode: "no-cors",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        Nome: nomeCategoria
      })
    })
      .then(function(result) {
        console.log("ok");
        console.log(result);
      })
      .catch(function() {
        console.log("error");
      });
    console.log("executou salvar!");
    console.log(nomeCategoria);
  }

  render() {
    return (
      <div
        id="atualizar-categoria-form"
        style={{
          display: "flex",
          flexDirection: "column",
          background: "#e9ecef",
          padding: "20px"
          //display: "none"
        }}
      >
        <span
          style={{
            color: "#563d7c",
            fontSize: "20px",
            textAlign: "center",
            fontWeight: "700",
            marginBottom: "10px"
          }}
        >
          Categoria
        </span>
        <div className="form-group">
          <div className="row">
            <div className="col-lg-12">
              <label>Nome</label>
              <input
                id="nome-categoria"
                className="form-control"
                style={{ marginBottom: "10px" }}
              />
            </div>
          </div>
          {/* <div className="row">
            <div className="col-lg-12">
              <label for="descricao-categoria">Descrição</label>
              <input
                id="descricao-categoria"
                className="form-control"
                style={{ marginBottom: "10px" }}
              />
            </div>
          </div> */}
          <div className="row">
            <div className="col-md-6">
              <button
                className="btn btn-primary form-control"
                onClick={() => this.LimparFormulario()}
                style={{ marginTop: "10px" }}
              >
                Limpar
              </button>
            </div>
            <div className="col-md-6">
              <button
                className="btn btn-primary form-control"
                onClick={() => this.salvarCategoria()}
                style={{ marginTop: "10px" }}
              >
                Salvar
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AtualizarCategoria;
