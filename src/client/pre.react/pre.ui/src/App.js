import React, { Component } from "react";
import AtualizarCategoria from "./components/categoria/atualizarcategoria.js";
import Aviso from "./components/aviso.js";
import NavigationBar from "./components/navigationComponents/navigationBar.js";
import WindowButtons from "./components/navigationComponents/windowButtons.js";
import FormSelector from "./components/navigationComponents/formSelectors.js";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import { faGlasses } from "@fortawesome/free-solid-svg-icons";
import AtualizarTopico from "./components/topico/atualizacaoTopico.js";

class App extends Component {
  state = {};
  render() {
    library.add(faCalendar);
    library.add(faGlasses);
    return (
      <div className="bg-light">
        <NavigationBar />
        <div
          style={{
            display: "flex",
            justifyContent: "space-around"
          }}
        >
          <WindowButtons />
          <div style={{ flex: 2, padding: "40px" }}>
            <Aviso />
          </div>
          <div style={{ flex: 1, padding: "40px" }}>
            <FormSelector />
            <AtualizarCategoria />
            <AtualizarTopico />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
